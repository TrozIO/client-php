<?php
namespace TrozIO;

class Client extends \KeenIO\Client\KeenIOClient
{

    public static function factory($config = array())
    {

        $default = array(
            'baseUrl'   => 'https://api.troz.io/3.0/',
            'version'   => '3.0',
            'masterKey' => null,
            'writeKey'  => null,
            'readKey'   => null,
            'projectId' => null,
        );
        // Create client configuration
        $config = self::parseConfig($config, $default);
        $config = \Guzzle\Common\Collection::fromConfig($config, $default);
        // Because each API Resource uses a separate type of API Key, we need to expose them all in
        // `commands.params`. Doing it this way allows the Service Definitions to set what API Key is used.
        $parameters = array();
        foreach (array('masterKey', 'writeKey', 'readKey') as $key) {
            $parameters[$key] = $config->get($key);
        }
        $config->set('command.params', $parameters);
        // Create the new Keen IO Client with our Configuration
        $client = new self($config->get('baseUrl'), $config);

        $file = 'keen-io-' . str_replace('.', '_', $client->getConfig('version')) . '.php';

        $resources = require __DIR__ . "/../../../keen-io/keen-io/src/Client/Resources/{$file}";

        $ops    = ["count", "countUnique", "minimum", "maximum", "average", "sum"];
        $newOps = [
            'latest'     => array(
                'location'    => 'query',
                'description' => 'An integer containing the number of most recent events to extract.',
                'type'        => 'number',
                'required'    => false,
            ),
            'order_by'   => array(
                'location'    => 'query',
                'description' => 'Intervals are used when creating a Series API call. The interval specifies the length of each sub-timeframe in a Series.',
                'type'        => 'string',
                'required'    => false,
            ),
            'arrange_by' => array(
                'location'    => 'query',
                'description' => 'The group_by parameter specifies the name of a property by which you would like to group the results.',
                'type'        => array('string', 'array'),
                'filters'     => array('\KeenIO\Client\Filter\MultiTypeFiltering::encodeValue'),
                'required'    => false,
            ),
        ];

        foreach ($ops as $op) {
            $resources["operations"][$op]["parameters"] = array_merge($resources["operations"][$op]["parameters"], $newOps);
        }

        $client->setDescription(\Guzzle\Service\Description\ServiceDescription::factory($resources));

        // Set the content type header to use "application/json" for all requests
        $client->setDefaultOption('headers', array('Content-Type' => 'application/json'));
        return $client;

    }

}
